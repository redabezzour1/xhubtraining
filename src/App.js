import React from 'react';
import Header from './components/HeaderComponent/header';
import Menu from './components/Menu/Menu';
import { Route, Switch } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className="App">
          <Switch>
            <Route path="/rents" component={Menu} />
            <Route path="/"  component={Header}/>
          </Switch>
    </div>
  );
}

export default App;
